class Student
  attr_reader :first_name, :last_name, :courses

  def initialize(first_name, last_name)
    @first_name = first_name.capitalize
    @last_name = last_name.capitalize
    @courses = []
  end

  def name
    name = "#{@first_name} #{@last_name}"
  end


  def course_load
    load = Hash.new { |hash, key| hash[key] = 0 }
    self.courses.each do |course|
      load[course.department] += course.credits
    end
    load
  end

  def conflicted(course2)
    self.courses.each do |enrolled|
      return true if course2.conflicts_with?(enrolled)
    end
    false
  end

  def enroll(courses)
    if conflicted(courses)
      raise "conflict!"
    end
    unless self.courses.include?(courses)
      self.courses << courses
      courses.students << self
    end
  end
end
